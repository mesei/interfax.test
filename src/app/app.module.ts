import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import { SafeHtmlPipe } from './pipes/safeHtml.pipe';

import { HeaderComponent } from './common/header.component';
import { FooterNavigationComponent } from './common/footer-navigation.component';

import { NewsComponent } from './news/news.component';
import { NewsItemComponent } from './news/news-item.component';

import '../styles/styles.scss';

import { BoardComponent } from './common/board.component';
import { NoContentComponent } from './no-content';

@NgModule({
  declarations: [
    AppComponent,
    NewsComponent,
    NewsItemComponent,
    HeaderComponent,
    FooterNavigationComponent,
    SafeHtmlPipe,
    BoardComponent,
    NoContentComponent
  ],
  imports: [
    RouterModule.forRoot(
      ROUTES
    ),
    BrowserModule,
    HttpModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'ru' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
