import { Routes } from '@angular/router';
import { NoContentComponent } from './no-content';

import { NewsComponent } from './news/news.component';

export const ROUTES: Routes = [
  { path: '',      component: NewsComponent },
  { path: 'news',  component: NewsComponent },
  { path: '**',    component: NoContentComponent },
];
