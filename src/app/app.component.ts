import { Component } from '@angular/core';

import { NewsService } from './services/news.service'
import { NewsItem } from './models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NewsService]
})
export class AppComponent {
  title = 'app';

  news: Array<NewsItem>;

  constructor(protected newsService: NewsService){
  }
}
