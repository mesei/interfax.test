import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import 'rxjs/Rx';

import { EventsItem } from '../models'

@Injectable()
export class EventsService{

    constructor(private http: Http){
        
    }

    getBirthdays(startDate: Date, endDate: Date): Promise<Array<EventsItem>>{
        let promise = new Promise<Array<EventsItem>>((resolve, reject) => {
            let apiURL = `/assets/mock-data/events.json`;
            this.http.get(apiURL)
                .toPromise()
                .then(
                    res => { // Success
                      let results = res.json().map(item => {
                        return new EventsItem(
                            item.Title,
                            new Date(item.StartDate),
                            item.PictureUrl
                        );
                      });
                      
                      resolve(results);
                    },
                    msg => { // Error
                      reject(msg);
                    }
                );
          });
          return promise;
    }
}