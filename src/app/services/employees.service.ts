import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import 'rxjs/Rx';

import { BirthdayItem } from '../models'

@Injectable()
export class EmployeesService{

    constructor(private http: Http){
        
    }

    getBirthdays(startDate: Date, endDate: Date): Promise<Array<BirthdayItem>>{
        let promise = new Promise<Array<BirthdayItem>>((resolve, reject) => {
            let apiURL = `/assets/mock-data/birthdays.json`;
            this.http.get(apiURL)
                .toPromise()
                .then(
                    res => { // Success
                      let results = res.json().map(item => {
                        return new BirthdayItem(
                            item.DisplayName,
                            item.FirstName,
                            item.LastName,
                            item.MiddleName,
                            new Date(item.Birthday),
                            item.Email,
                            item.PictureUrl
                        );
                      });
                      
                      resolve(results);
                    },
                    msg => { // Error
                      reject(msg);
                    }
                );
          });
          return promise;
    }
}