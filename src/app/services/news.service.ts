import { Injectable } from '@angular/core'
import { Http } from '@angular/http'

import { NewsItem } from '../models'
import 'rxjs/Rx';
import { Title } from '@angular/platform-browser/src/browser/title';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class NewsService{

    constructor(private http: Http){
        
    }

    getNews(): Promise<Array<NewsItem>>{
        let promise = new Promise<Array<NewsItem>>((resolve, reject) => {
            let apiURL = `/assets/mock-data/news.json`;
            this.http.get(apiURL)
                .toPromise()
                .then(
                    res => { // Success
                      let results = res.json().map(item => {
                        return new NewsItem(
                            item.Title,
                            new Date(item.PublishDate),
                            item.Preview,
                            item.City,
                            item.Source,
                            item.Text,
                            item.Pictures
                        );
                      });
                      
                      resolve(results);
                    },
                    msg => { // Error
                      reject(msg);
                    }
                );
          });
          return promise;
    }

    getNewsById(id: number): Promise<NewsItem>{
        
        let promise = new Promise<NewsItem>((resolve, reject) => {
            let apiURL = `/assets/mock-data/news.json`;
            this.http.get(apiURL)
                .toPromise()
                .then(
                    res => {
                        let results = res.json().map(item => {
                            console.log(item);
                        return new NewsItem(
                            item.Title,
                            new Date(item.PublishDate),
                            item.Preview,
                            item.City,
                            item.Source,
                            item.Text,
                            item.Pictures
                        );
                      });
                      
                      resolve(results[0]);
                    },
                    msg => {
                      reject(msg);
                    }
                );
          });
          return promise;
    }
}