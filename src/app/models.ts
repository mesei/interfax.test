export class NewsItem{

    constructor(
        public Title: string,
        public PublishDate: Date,
        public Preview: string,
        public City: string,
        public Source: string,
        public Text: string,
        public Pictures: Array<string>
    ){

    }
}

export class CalendarItem{

    constructor(
        public Title: string,
        public StartDate: Date,
        public Description: string,
        public PictureUrl: string
    ){

    }
}

export class BirthdayItem{

    public Color: string;

    constructor(
        public DisplayName: string,
        public FirstName: string,
        public LastName: string,
        public MiddleName: string,
        public Birthday: Date,
        public Email: string,
        public PictureUrl: string
    ){

        this.Color = BirthdayItem.GetRandomColor();
    }

    public static GetRandomColor(){
        
        // Чтобы не было совсм кислотных цветов
        let colors = [
            Math.floor(Math.random()*100) + 100,
            Math.floor(Math.random()*150) + 100,
            Math.floor(Math.random()*255)
        ];

        return `rgb(${colors.join(',')})`;
    }
}

export class EventsItem{
    constructor(
        public Title: string,
        public StartDate: Date,
        public PictureUrl: string
    ){

    }
} 