import { Component } from '@angular/core';

import { NewsService } from '../services/news.service'
import { NewsItem } from '../models';
import { OnInit } from '@angular/core';

@Component({
  selector: 'news',
  templateUrl: './news.component.html',
  //styleUrls: ['./news.component.css'],
  providers: [NewsService]
})
export class NewsComponent implements OnInit {

  newsItem: NewsItem;

  constructor(protected newsService: NewsService){
    this.newsService.getNewsById(1).then((data: NewsItem) => {
      this.newsItem = data;
    });
  }

  ngOnInit(){
  }
}
