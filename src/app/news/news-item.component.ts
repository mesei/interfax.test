import { Component, Input } from '@angular/core';

import { NewsItem } from '../models';

@Component({
  selector: 'news-item',
  templateUrl: './news-item.component.html',
  //styleUrls: ['./news.component.css'],
  providers: []
})
export class NewsItemComponent {
  @Input()
  item: NewsItem;

  constructor(){
  }
}
