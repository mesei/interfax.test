import { Component } from '@angular/core';
import * as moment from 'moment';


import { EmployeesService } from '../services/employees.service';
import { BirthdayItem, EventsItem } from '../models';
import { Moment, LocaleSpecification, CalendarSpec } from 'moment';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'board',
  templateUrl: './board.component.html',
  //styleUrls: ['./news.component.css'],
  providers: [EmployeesService, EventsService]
})
export class BoardComponent {

  countDays: number = 3;

  selectedTab: number = 0;

  dates: Array<any> = [];

  birthdays: { [ key: string] : Array<BirthdayItem> } = {};

  events: Array<EventsItem>;

  constructor(private employeesService: EmployeesService,
    private eventsService: EventsService
  ){

    let startDate = moment.utc();
      let endDate = moment.utc();
    for(let i=0; i< this.countDays; i++){
        this.dates.push(moment.utc(endDate));
        endDate.add("days", 1);
    }

    this.employeesService.getBirthdays(startDate.toDate(), endDate.toDate())
      .then((birthdays: Array<BirthdayItem>) => {
          birthdays.forEach((el: BirthdayItem) => {
              if (!this.birthdays[el.Birthday.getDate()]) {
                  this.birthdays[el.Birthday.getDate()] = new Array();
              }
              this.birthdays[el.Birthday.getDate()].push(el);
          });
      });

      this.eventsService.getBirthdays(startDate.toDate(), startDate.toDate())
      .then((events: Array<EventsItem>) => {
        this.events = events;
      });
  }

  public TabClick(event, tabNumber){
    event.preventDefault();

    this.selectedTab = tabNumber;
  }

  private getDayString(index: number): string{
    switch(index){
      case 0:
        return "Сегодня";
      case 1:
        return "Завтра";
      case 2:
        return "Послезавтра";
    }
  }
}
